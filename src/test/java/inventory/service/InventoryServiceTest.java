package inventory.service;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class InventoryServiceTest {
    @Spy
    private InventoryRepository inventoryRepository;
    @InjectMocks
    private InventoryService inventoryService;
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void lookupPart() {
        Part part = new OutsourcedPart(1, "jjllj", 2.1, 4, 1, 6, "ColySRL");
        assert inventoryService.lookupPart(part.getName()) == null;
    }
    @Test
    public void addPart() {
        inventoryService.addOutsourcePart( "jjlj", 2.1, 4, 1, 6, "ColySRL");
        assert inventoryService.lookupPart("jjlj").getName().equals("jjlj");
    }

}