package inventory.service;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
@ExtendWith(MockitoExtension.class)
public class ServiceRepoTestIntegration {
    @Spy
    private InventoryRepository inventoryRepository;
    @InjectMocks
    private InventoryService inventoryService;


    @Test
    public void lookupPart() {
        Part part = new OutsourcedPart(1, "asd", 2.1, 4, 1, 6, "ColySRL");
        assert inventoryRepository.lookupPart(part.getName()) == null;
        Mockito.verify(inventoryRepository, times(1)).lookupPart(any());
    }
    @Test
    public void addPart() {
        Part part = new OutsourcedPart(0, "fgh", 2.1, 4, 1, 6, "ColySRL");
        inventoryService.addOutsourcePart( part.getName(), 2.1, 4, 1, 6, "ColySRL");
        assert inventoryService.lookupPart(part.getName()).getName().equals(part.getName());
    }
}