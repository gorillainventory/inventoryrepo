package inventory.service;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import inventory.repository.InventoryRepository;
import inventory.service.InventoryService;
import org.junit.Before;

import org.junit.jupiter.api.extension.ExtendWith;


import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import org.junit.jupiter.api.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
@ExtendWith(MockitoExtension.class)
public class InventoryServiceTestIntegration {
    @Mock
    private InventoryRepository inventoryRepository;
    @InjectMocks
    private InventoryService inventoryService;
    @Test
    public void lookupPart() {
        Part part = new OutsourcedPart(1, "qwe", 2.1, 4, 1, 6, "ColySRL");
        assert inventoryRepository.lookupPart(part.getName()) == null;
        Mockito.verify(inventoryRepository, times(1)).lookupPart(any());
    }

    @Test
    public void addPart() {
        inventoryService.addOutsourcePart( "zxc", 2.1, 4, 1, 6, "ColySRL");
        Mockito.verify(inventoryRepository, times(1)).addPart(any());

    }
}