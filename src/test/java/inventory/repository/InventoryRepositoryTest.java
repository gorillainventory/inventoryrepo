package inventory.repository;

import inventory.model.OutsourcedPart;
import inventory.model.Part;

import inventory.repository.InventoryRepository;
import org.junit.Before;


import org.junit.Test;
import org.junit.jupiter.api.Order;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
public class InventoryRepositoryTest {
    @Spy
    private InventoryRepository inventoryRepository;
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void lookupPart() {
        Part part = new OutsourcedPart(1, "jjll", 2.1, 4, 1, 6, "ColySRL");
        assert inventoryRepository.lookupPart(part.getName()) == null;
    }


    @Test
    public void addPart() {
        Part part = new OutsourcedPart(1, "jjjl", 2.1, 4, 1, 6, "ColySRL");
        inventoryRepository.addPart(part);
        assert inventoryRepository.lookupPart(part.getName()).getName().equals(part.getName());
    }
}