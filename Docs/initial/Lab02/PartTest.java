package inventory.model;

import static org.junit.jupiter.api.Assertions.*;

class PartTest {

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void isValidPartECP1() {
        String err;
        err = Part.isValidPart("Name", 0.20, 2010, 1, 2020, "");
        assert err.equals("");
    }

    @org.junit.jupiter.api.Test
    void isValidPartBVA1() {
        String err;
        err = Part.isValidPart("M", 10.00, 1, 1, 2, "");
        assert err.equals("");
    }

    @org.junit.jupiter.api.Test
    void isValidPartBVA2() {

        String err;
        err = Part.isValidPart("", 10.00, 1, 1, 2, "");
        assert err.equals("A name has not been entered. ");
    }

    @org.junit.jupiter.api.Test
    void isValidPartBVA3() {
        String err;
        err = Part.isValidPart("M", 10.00, 1, 1, 1, "");
        assert err.equals("");
    }
}
