package inventory.model;

import inventory.model.OutsourcedPart;
import inventory.model.Part;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;


public class PartTest2 {

    @BeforeEach
    public void setUp() {

    }

    @Test
   public void getMax() {
        Part part=new OutsourcedPart(1,"Name",2.1,3,1,4, "ColySRL");
        assert part.getMax()==4;
    }

    @Test
    public void setMax() {
        Part part=new OutsourcedPart(1,"Name",2.1,3,1,4, "ColySRL");
        part.setMax(10);
        assert part.getMax()==10;
    }

    @Test
   public void testToString() {
        Part part=new OutsourcedPart(1,"Name",2.1,3,1,4, "ColySRL");
        System.out.println(part.toString());
        assert part.toString().equals("O,1,Name,2.1,3,1,4,ColySRL");
    }
}