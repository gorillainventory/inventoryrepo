package inventory.model;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Timeout;

class PartWBTest {

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
    }

    @org.junit.jupiter.api.AfterEach
    void tearDown() {
    }

    @org.junit.jupiter.api.Test
    void isValidPart1() {
        String err;
        err = Part.isValidPart("", 0, 0, 1, -1, "");
        assert err.equals("A name has not been entered. The price must be greater than 0. Inventory level must be greater than 0. The Min value must be less than the Max value. Inventory level is lower than minimum value. Inventory level is higher than the maximum value. ");
    }

    @org.junit.jupiter.api.Test
    void isValidPart2() {
        String err;
        err = Part.isValidPart("Name", 0.20, 2010, 1, 2020, "");
        assert err.equals("");
    }

    @org.junit.jupiter.api.Test
    void isValidPart3() {
        String err;
        err = Part.isValidPart("", 0, 0, 0, -1, "");
        assert err.equals("A name has not been entered. The price must be greater than 0. Inventory level must be greater than 0. The Min value must be less than the Max value. Inventory level is higher than the maximum value. ");
    }
}
